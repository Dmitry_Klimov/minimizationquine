package my;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

public class Printer {
    public Printer() {}

    public void printIntArray(int[] array){ //print int Array
        for (int i: array) {
            System.out.print(i);
        }
        System.out.println();
    }

    public void printerStringArray(String[] strings){
        for (int i = 0; i < strings.length; i++) {
            System.out.print(strings[i]);
        }
        System.out.println();
    }

    public void printerKeyTypeString(Map map){
        Set keys = map.keySet();
        for (Object key: keys) {
            System.out.print((String)map.get(key));
        }
        System.out.println();
    }

    public void printMap(Map mainMap){ //print Main Map
        Set mainKeys = mainMap.keySet();
        for (Object key: mainKeys) {
            printIntArrayList((ArrayList) key);
            String[] valueStringArray = (String[]) mainMap.get(key);
            for (int i = 0; i < valueStringArray.length; i++) {
                System.out.print(valueStringArray[i]);
            }
            System.out.println();
        }
        System.out.println();
    }

    public void printIntArrayList(ArrayList list){ // print int ArrayList
        System.out.println(list);
    }

    public void printArrayArraysWithFormat(ArrayList list){ // print int ArrayList
        for (int i = 0; i < list.size(); i++) {
            ArrayList temp = (ArrayList) list.get(i);
            for (int j = 0; j < temp.size(); j++) {
                System.out.print(temp.get(j));
                if(j != temp.size() - 1){
                    System.out.print("-");
                }
            }
            System.out.println();
        }
        System.out.println();
    }

    public void printMapInsideMap(Map map){ //print Map
        Set keys = map.keySet();
        for (Object key: keys) {
            System.out.print(key);
        }
        System.out.println();
        for (Object key: keys) {
            System.out.print((int)map.get(key));
        }
        System.out.println();
    }

    public void printStringArrayList(ArrayList<String> list){
        for (String str: list) {
            System.out.print(str);
        }
        System.out.println();
    }

    public void printArrayListStringArrays(ArrayList list){
        for (int i = 0; i < list.size(); i++) {
            String[] temp = (String[]) list.get(i);
            for (int j = 0; j < temp.length; j++) {
                System.out.print(temp[j]);
            }
            System.out.println();
        }
    }

    public void printThoLevelArray(int[][] table, int size1, int size2){
        for (int i = 0; i < size1; i++) {
            for (int j = 0; j < size2; j++) {
                if (j == size2 - 1){
                    System.out.println(table[i][j]);
                    continue;
                }
                System.out.print(table[i][j] + "\t");
            }

        }
    }

}
