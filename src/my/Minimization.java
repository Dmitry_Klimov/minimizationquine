package my;

import java.util.ArrayList;
import java.util.Map;

public class Minimization {

    public Minimization() {
        minimization();
    }

    public void minimization() {
        DoWork doWork = new DoWork();
        Printer printer = new Printer();

        Map binaryFunction = doWork.createMap();
        printer.printMap(binaryFunction);
        System.out.println(binaryFunction.size());
        System.out.println("------------------------");
        ArrayList keysGluing;
        do {
            keysGluing = doWork.searchGluingNum(binaryFunction);
            printer.printArrayArraysWithFormat(keysGluing);
            if (keysGluing.size() != 0) {
                System.out.println("------------------------");
                binaryFunction = doWork.gluing(binaryFunction);
                printer.printMap(binaryFunction);
                System.out.println(binaryFunction.size());
                System.out.println("------------------------");
            }

        }while (keysGluing.size() != 0);

        ArrayList answer = doWork.overlappingFull(binaryFunction);
        //ArrayList answer = doWork.overlappingPartially(binaryFunction);
        printer.printArrayListStringArrays(answer);

        doWork.truthTable();
    }
}
