package my;

import java.util.*;

import static java.lang.Math.*;

public class DoWork {

    private final int NUM_VARIABLE = 4;
    private final int[] FUNCTION = {0, 1, 6, 7, 8, 11, 12, 14};//{0, 1, 2, 3, 4, 5, 6, 7, 11, 13, 14}{0, 1, 6, 7, 8, 11, 12, 14}
    private final int[] FUNCTION_NULL = {6, 7, 11, 13, 14};

    private ArrayList defaultFunction = new ArrayList<String[]>();
    private ArrayList gluingFunction = new ArrayList<String>();
    private int[][] tableOverlapping;

    private ArrayList answer = new ArrayList<String[]>();
    private int[][] truthTable;

    Printer printer = new Printer();

    public int[] convertToBinary(int dec){
        String str = Integer.toBinaryString(dec); //Integer to BinaryString
        char[] chr = str.toCharArray(); //convert to char Array
        if(chr.length > NUM_VARIABLE){
            System.out.println("Error, incorrect function value.");
            System.exit(0);
        }
        int[] bin = new int[NUM_VARIABLE]; //convert char Array to int Array
        for (int i: bin) bin[i] = 0;
        int temp = NUM_VARIABLE - 1;
        for (int i = chr.length - 1; i >= 0; i--) {
            bin[temp] = Character.digit(chr[i],2);
            temp--;
        }
        return bin; //return int Array
    }

    public String[] createMapBinaryValue(int[] temp){
        String[] value = new String[NUM_VARIABLE];
        for (int i = 0; i < NUM_VARIABLE; i++) {
            if(temp[i] == 0){
                value[i] = String.format("!X%s", i + 1);
            }else {
                value[i] = String.format("X%s", i + 1);
            }
        }
        return value;
    }

    public Map createMap(){ //create Map: key - ArrayList, value - Map
        Map binaryFunction = new LinkedHashMap<ArrayList, Map>();
        for (int i = 0; i < FUNCTION.length; i++) {
            ArrayList list = new ArrayList<Integer>();
            int[] temp = convertToBinary(FUNCTION[i]); // convertToBinary and create int Array
            list.add(FUNCTION[i]);//create ArrayList
            binaryFunction.put(list, createMapBinaryValue(temp)); // add Map
        }
        return binaryFunction;
    }

    public int compareStringArray(String[] first, String[] second, int minimumNumberVariables){
        int alignment = 0;
        if (first.length == minimumNumberVariables) {
            for (int i = 0; i < first.length; i++) {
                if (first[i].equals(second[i])) {
                    alignment++;
                }
            }
        }
        if (alignment == minimumNumberVariables - 1) {
            for (int i = 0; i < first.length; i++) {
                if(!first[i].equals(second[i])){
                    if (compareStrings(first[i], second[i]) || first.length != second.length) {
                        alignment = 0;
                    }
                }
            }
        }
        return alignment;
    }

    public boolean compareStrings(String str1, String str2){
        boolean suit = true;
        if (str1.charAt(str1.length() - 1) == str2.charAt(str2.length() - 1)) {
            suit = false;
        }
        return suit;
    }

    public int searchMinimumNumberVariables(Map binaryFunction){
        Set keys = binaryFunction.keySet();
        int minVars = 0;
        for (Object key: keys) {
            String[] strInside = (String[]) binaryFunction.get(key);
            if (minVars == 0){
                minVars = strInside.length;
            }
            int numVars = strInside.length;
            if (minVars > numVars){
                minVars = numVars;
            }
        }
        return  minVars;
    }

    public ArrayList searchGluingNum(Map binaryFunction){ // return number gluing parts ArrayArrays
        ArrayList gluingNum = new ArrayList<ArrayList>();
        Set keys = binaryFunction.keySet();
        ArrayList function = new ArrayList(keys);
        int minimumNumberVariables = searchMinimumNumberVariables(binaryFunction);
        int alignment;
        for (int i = 0; i < function.size() - 1; i++) {
            for (int j = i + 1; j < function.size(); j++) {
                alignment = compareStringArray((String[])binaryFunction.get(function.get(i)), (String[])binaryFunction.get(function.get(j)), minimumNumberVariables);
                if (alignment == minimumNumberVariables - 1 && minimumNumberVariables - 1 != 0) {
                    ArrayList temp = new ArrayList<ArrayList>();
                    temp.add(function.get(i));
                    temp.add(function.get(j));
                    gluingNum.add(temp);
                }
            }
        }

        return gluingNum;
    }
    
    public String[] gluingString(String[] strFirst, String[] strSecond, int minimumNumberVariables){
        String[] str = new String[minimumNumberVariables - 1];
        if (strFirst.length != strSecond.length){
           System.out.println("gluingMaps() error");
           System.exit(0);
        }
        int j = 0;
        for (int i = 0; i < strFirst.length; i++) {
            if (strFirst[i].equals(strSecond[i])){
                str[j] = strFirst[i];
                j++;
            }
        }
        return str;
    }

    public ArrayList thoArrayListInOne(ArrayList first, ArrayList second){
        ArrayList list = new ArrayList<Integer>(first);
        list.addAll(second);
        return list;
    }

    public Map gluing(Map map){
        ArrayList gluingNum = searchGluingNum(map); // number gluing parts ArrayListArraysLists
        Map glued = new LinkedHashMap<ArrayList, Map>(); //return
        int minimumNumberVariables = searchMinimumNumberVariables(map);

        for (int i = 0; i < gluingNum.size(); i++) {
            ArrayList keys = (ArrayList) gluingNum.get(i);
            if (keys.size() >= 3) {
                System.out.println("gluind(): arrays > 3, error");
                System.exit(0);
            }
            ArrayList firstKey = (ArrayList) keys.get(0);
            ArrayList secondKey = (ArrayList) keys.get(1);

            String[] strInsideFirst = (String[]) map.get(firstKey);
            String[] srtInsideSecond = (String[]) map.get(secondKey);

            ArrayList newKey = thoArrayListInOne(firstKey, secondKey);
            String[] strInside = gluingString(strInsideFirst, srtInsideSecond, minimumNumberVariables);

            glued.put(newKey, strInside);
        }
        addNoGlued(gluingNum, map, glued);
        deleteCopies(glued);
        return glued;
    }

    public void addNoGlued(ArrayList gluingNum, Map mapStart, Map glued){
        Set keysMap = mapStart.keySet();
        int alignment = 0;
        for (Object key: keysMap) {
            for (int i = 0; i < gluingNum.size(); i++) {
                ArrayList temp = (ArrayList) gluingNum.get(i);
                for (int j = 0; j < temp.size(); j++) {
                    if (key.equals(temp.get(j))){
                        alignment++;
                    }
                }
            }
            if (alignment == 0){
                glued.put(key, mapStart.get(key));
            }
            alignment = 0;
        }
    }

    public void deleteCopies(Map glued){
        Set keys = glued.keySet();
        ArrayList function = new ArrayList(keys);
        ArrayList keyRemove = new ArrayList<ArrayList>();
        int minimumNumberVariables = searchMinimumNumberVariables(glued);
        int alignment;
        for (int i = 0; i < function.size() - 1; i++) {
            for (int j = i + 1; j < function.size(); j++) {
                alignment = compareValueStringArraysDeleteCopies((String[])glued.get(function.get(i)), (String[])glued.get(function.get(j)), minimumNumberVariables);
                if (alignment == minimumNumberVariables) {
                    keyRemove.add(function.get(j));
                }
            }
        }
        cleanKeyRemove(keyRemove);
        for (int i = 0; i < keyRemove.size(); i++) {
            Iterator<ArrayList> iterator = keys.iterator();
            while (iterator.hasNext()) {
                ArrayList list = iterator.next();
                if (compareArrayLists((ArrayList) keyRemove.get(i), list)) {
                    glued.remove(list);
                    keys = glued.keySet();
                    break;
                }
            }
        }
    }

    public boolean compareArrayLists(ArrayList first, ArrayList second){
        boolean suit = true;
        if(first.size() != second.size()){
            System.out.println("compareArrayLists() error");
            System.exit(0);
        }
        for (int i = 0; i < first.size(); i++) {
            if (first.get(i) != second.get(i)){
                suit = false;
            }
        }

        return suit;
    }

    public int compareValueStringArraysDeleteCopies(String[] strFirst, String[] strSecond, int minimumNumberVariables){ // compare int Array and return number of identical parts
        int alignment = 0;

        if (strFirst.length == strSecond.length && strFirst.length == minimumNumberVariables) {
            for (int i = 0; i < strFirst.length; i++) {
                if (strFirst[i].equals(strSecond[i])) {
                    alignment++;
                }
            }
        }
        return alignment;
    }

    public void cleanKeyRemove(ArrayList keyRemove){
        for (int i = 0; i < keyRemove.size() - 1; i++) {
            for (int j = i + 1; j < keyRemove.size(); j++) {
                if (compareArrayLists((ArrayList) keyRemove.get(i), (ArrayList) keyRemove.get(j))){
                    keyRemove.remove(j);
                }
            }
        }
    }

    public ArrayList overlappingPartially(Map binaryFunction){
        for (int i = 0; i < FUNCTION_NULL.length; i++) {
            int[] temp = convertToBinary(FUNCTION_NULL[i]); // convertToBinary and create int Array
            String[] initialFunction = new String[NUM_VARIABLE];
            for (int j = 0; j < NUM_VARIABLE; j++) {
                if(temp[j] == 0){
                    initialFunction[j] = String.format("!X%s", j + 1);
                }else {
                    initialFunction[j] = String.format("X%s", j + 1);
                }
            }
            defaultFunction.add(initialFunction);
        }

        Set keys = binaryFunction.keySet();
        for (Object key: keys) {
            gluingFunction.add(binaryFunction.get(key));
        }
        tableOverlapping= new int[gluingFunction.size()][defaultFunction.size()];
        for (int i = 0; i < gluingFunction.size(); i++) {
            for (int j = 0; j < defaultFunction.size(); j++) {
                tableOverlapping[i][j] = compareStringArray((String[]) gluingFunction.get(i), (String[]) defaultFunction.get(j));
            }
        }
        printer.printThoLevelArray(tableOverlapping, gluingFunction.size(), defaultFunction.size());
        ArrayList answer = answer(tableOverlapping, gluingFunction, defaultFunction);
        return answer;
    }

    public ArrayList overlappingFull(Map binaryFunction){
        ArrayList defaultFunction = new ArrayList<String[]>();
        for (int i = 0; i < FUNCTION.length; i++) {
            int[] temp = convertToBinary(FUNCTION[i]); // convertToBinary and create int Array
            String[] initialFunction = new String[NUM_VARIABLE];
            for (int j = 0; j < NUM_VARIABLE; j++) {
                if(temp[j] == 0){
                    initialFunction[j] = String.format("!X%s", j + 1);
                }else {
                    initialFunction[j] = String.format("X%s", j + 1);
                }
            }
            defaultFunction.add(initialFunction);
        }

        ArrayList gluingFunction = new ArrayList<String>();
        Set keys = binaryFunction.keySet();
        for (Object key: keys) {
            gluingFunction.add(binaryFunction.get(key));
        }

        tableOverlapping = new int[gluingFunction.size()][defaultFunction.size()];
        for (int i = 0; i < gluingFunction.size(); i++) {
            for (int j = 0; j < defaultFunction.size(); j++) {
                tableOverlapping[i][j] = compareStringArray((String[]) gluingFunction.get(i), (String[]) defaultFunction.get(j));
            }
        }
        printer.printThoLevelArray(tableOverlapping, gluingFunction.size(), defaultFunction.size());
        ArrayList answer = answer(tableOverlapping, gluingFunction, defaultFunction);
        return answer;
    }

    public int compareStringArray(String[] first, String[] second){
        int alignment = 0;
        if (first.length <= second.length) {
            for (int i = 0; i < first.length; i++) {
                for (int j = 0; j < second.length; j++) {
                    if (first[i].equals(second[j])) {
                        alignment++;
                    }
                }
            }
        }
        if (alignment == first.length){
            return 1;
        }else
            return 0;
    }

    public ArrayList answer(int[][] tableOverlapping, ArrayList gluingFunction, ArrayList defaultFunction){
        ArrayList temp = new ArrayList<Integer>();
        for (int i = 0; i < defaultFunction.size(); i++) {
            ArrayList countJ = new ArrayList<Integer>();
            for (int j = 0; j < gluingFunction.size(); j++) {
                if (tableOverlapping[j][i] == 1) {
                    countJ.add(j);
                }
            }
            if (countJ.size() == 1) {
                temp.add(countJ.get(0));
                answer.add(gluingFunction.get((Integer) countJ.get(0)));
            }
        }
        for (int i = 0; i < defaultFunction.size(); i++) {
            int exit = 0;
            ArrayList countJ = new ArrayList<Integer>();
            for (int j = 0; j < gluingFunction.size(); j++) {
                if (tableOverlapping[j][i] == 1) {
                    countJ.add(j);
                }
            }
            if (countJ.size() > 1) {
                for (int j = 0; j < countJ.size(); j++) {
                    for (int k = 0; k < temp.size(); k++) {
                        if(countJ.get(j) == temp.get(k)){
                            exit++;
                        }
                    }
                }
                if (exit == 0) {
                    int[] countOne = new int[countJ.size()];
                    for (int j = 0; j < countJ.size(); j++) {
                        int counter = 0;
                        for (int k = 0; k < defaultFunction.size(); k++) {
                            if (tableOverlapping[j][k] == 1) {
                                counter++;
                            }
                        }
                        countOne[j] = counter;
                    }
                    int max = countOne[0];
                    int index = 0;
                    for (int j = 0; j < countOne.length; j++) {
                        if (max < countOne[j]) {
                            max = countOne[j];
                            index = j;
                        }
                    }
                    answer.add(gluingFunction.get((Integer)countJ.get(index)));
                }
            }
        }
        deleteCopiesInAnswer();
        return answer;
    }

    public void deleteCopiesInAnswer(){
        for (int i = 0; i < answer.size() - 1; i++) {
            for (int j = i + 1; j < answer.size(); j++) {
                if (answer.get(i).equals(answer.get(j))){
                    answer.remove(j);
                }
            }
        }
    }

    public void truthTable(){
        int height = (int) pow(2, NUM_VARIABLE);
        truthTable = new int[height][NUM_VARIABLE + 2];
        for (int i = 0; i < height; i++) {
            truthTable[i][0] = i;
            int[] bin = convertToBinary(i);
            for (int j = 1; j < NUM_VARIABLE + 1; j++) {
                truthTable[i][j] = bin[j - 1];
                if (j == NUM_VARIABLE){
                    truthTable[i][j + 1] = functionValue(bin);
                }

            }
        }
        printer.printThoLevelArray(truthTable, height, NUM_VARIABLE + 2);
    }

    public int functionValue(int[] bin){
        int functionValue = 0;
        for (int i = 0; i < answer.size(); i++) {
            String[] function = (String[]) answer.get(i);
            for (int j = 0; j < function.length; j++) {
                String str = function[j];
                int index = Integer.valueOf(String.valueOf(str.charAt(str.length() - 1)));
                int value = bin[index - 1];
                if (str.charAt(0) == '!'){
                    if (value == 0) value = 1;
                    else value = 0;
                }
                if (value == 0){
                    break;
                }
                if (j == function.length - 1){
                    functionValue = 1;
                }
            }
        }
        return functionValue;
    }

}
